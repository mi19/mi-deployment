#!/usr/bin/env bash
set -eu
# system parameters
SCRIPT_DIR=$(dirname $0)
PYTHON_EXE=$(which python2.7)




# second level params
PROJECT_ROOT='/opt/mi/'

# third level params
DJANGO_APP_HOME="$PROJECT_ROOT/django_app"
CI_SERVER_HOME="$PROJECT_ROOT/buildbot"


# helpers params
DJANGO_APP_VENV_PATH="$DJANGO_APP_HOME"
DJANGO_APP_SRC_PATH="$DJANGO_APP_HOME/src/"
CI_SERVER_VENV_PATH="$CI_SERVER_HOME/"

# buildbot params
CI_USER='mi'
BUILDBOT="$CI_SERVER_VENV_PATH/bin/buildbot"
BUILDSLAVE="$CI_SERVER_VENV_PATH/bin/buildslave"
CI_SERVER_MASTER_PATH="$CI_SERVER_HOME/master"
CI_SERVER_MASTER_ADDRESS="localhost:9989"
CI_SERVER_SLAVE_PATH="$CI_SERVER_HOME/slave"
CI_SERVER_SLAVE_NAME="django_app_build_slave"
CI_SERVER_SLAVE_PWD="mi_fonar"




[ -f $SCRIPT_DIR/settings.sh ] && source $SCRIPT_DIR/settings.sh

# script starts here
_log(){
    echo "###### $@"
}

setup_py_utils(){
    _log "START SETUP PYTHON UTILS"
    _log ""
    _log ""
    yum install -y python-pip
    yum install -y python-virtualenv
    _log ""
    _log ""
}


create_venv() {
    local venv_path="$1"
    local requirements_path="$2"
    if [ ! -f $venv_path/bin/activate ]; then
        _log "Create virtualenv $venv_path"
        virtualenv -p $PYTHON_EXE $venv_path
    fi
    (
        _log "install python modules from $requirements_path"
        $venv_path/bin/pip install -r $requirements_path
    )
}


replace_string() {
    local pattern_code=$1
    local value=$2
    local file=$3
    sed -i -e "s|\%\%\%\%$1\%\%\%\%|"$2"|g" $file
}

prepare_master_service_file(){
    local fp_src="$SCRIPT_DIR/buildbot_master.service.tmpl"
    local fp="$SCRIPT_DIR/buildbot_master.service"
    local dst_service_path="/usr/lib/systemd/system/buildbot_master.service"
    cp $fp_src $fp
    cat $fp
    replace_string PIDPATH $CI_SERVER_MASTER_PATH/twistd.pid $fp
    replace_string HOMEDIR $(dirname "$CI_SERVER_MASTER_PATH/../") $fp
    replace_string WORKDIR "$CI_SERVER_MASTER_PATH" $fp
    replace_string BUILDBOT_EXEC "$BUILDBOT" $fp
    replace_string USER $CI_USER $fp
    cp $fp $dst_service_path
    chmod 644 $dst_service_path
    chown root:root $dst_service_path
    chown -Rf $CI_USER:$CI_USER $CI_SERVER_MASTER_PATH
    cat $dst_service_path
}

prepare_slave_service_file(){
    local fp_src="$SCRIPT_DIR/buildbot_slave.service.tmpl"
    local fp="$SCRIPT_DIR/buildbot_slave.service"
    local dst_service_path="/usr/lib/systemd/system/buildbot_slave.service"
    cp $fp_src $fp
    cat $fp
    replace_string PIDPATH $CI_SERVER_SLAVE_PATH/twistd.pid $fp
    replace_string HOMEDIR $(dirname "$CI_SERVER_SLAVE_PATH/../") $fp
    replace_string WORKDIR "$CI_SERVER_SLAVE_PATH" $fp
    replace_string BUILDBOT_EXEC "$BUILDSLAVE" $fp
    replace_string USER $CI_USER $fp
    cp $fp $dst_service_path
    chmod 644 $dst_service_path
    chown root:root $dst_service_path
    chown -Rf $CI_USER:$CI_USER $CI_SERVER_SLAVE_PATH
    cat $dst_service_path
}


try_to_stop_services() {
    systemctl stop buildbot_master.service || true
    systemctl stop buildbot_slave.service || true
}

run_services() {
    systemctl start buildbot_master.service
    systemctl start buildbot_slave.service

}

setup_buildbot(){
    $BUILDBOT create-master $CI_SERVER_MASTER_PATH
    rsync -av $SCRIPT_DIR/master.cfg $CI_SERVER_MASTER_PATH/
    for dst_path in /etc/rc.d/ /etc/init.d/; do
        cp $SCRIPT_DIR/rc.d/buildbot_master.sh $dst_path
    done
    $BUILDSLAVE create-slave $CI_SERVER_SLAVE_PATH $CI_SERVER_MASTER_ADDRESS $CI_SERVER_SLAVE_NAME $CI_SERVER_SLAVE_PWD
}


main(){
    setup_py_utils
    create_venv $CI_SERVER_VENV_PATH $SCRIPT_DIR/requirements.txt
    _log ""
    _log ""
    _log "Start seting up buildbot"
    _log ""
    _log ""
    setup_buildbot
    _log ""
    _log ""
    _log "buildbot setted up"
    _log ""
    _log ""
    try_to_stop_services
    prepare_master_service_file
    prepare_slave_service_file
    _log "All ok! Service files created. Try to reload."
    systemctl daemon-reload && _log "Ok reloaded services" || (_log "Failed reloaded services" && exit 1)
    set -x
    run_services
    set +x
    systemctl status buildbot_slave.service
}

main $@