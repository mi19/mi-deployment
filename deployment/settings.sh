#!/usr/bin/env bash

# second level params
PROJECT_ROOT='/opt/mi/'

# third level params
DJANGO_APP_HOME="$PROJECT_ROOT/django_app"
CI_SERVER_HOME="$PROJECT_ROOT/buildbot"


# helpers params
DJANGO_APP_VENV_PATH="$DJANGO_APP_HOME"
DJANGO_APP_SRC_PATH="$DJANGO_APP_HOME/src/"
CI_SERVER_VENV_PATH="$CI_SERVER_HOME/"

# buildbot params
BUILDBOT="$CI_SERVER_VENV_PATH/bin/buildbot"
BUILDSLAVE="$CI_SERVER_VENV_PATH/bin/buildslave"
CI_SERVER_MASTER_PATH="$CI_SERVER_HOME/master"
CI_SERVER_MASTER_ADDRESS="localhost:9989"
CI_SERVER_SLAVE_PATH="$CI_SERVER_HOME/slave"
CI_SERVER_SLAVE_NAME="django_app_build_slave"
CI_SERVER_SLAVE_PWD="mi_fonar"
